module.exports = {
  reactStrictMode: true,
  env: {
    API_HOST: process.env.API_HOST
  }
}
