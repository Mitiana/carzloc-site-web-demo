export let ReturnCode = {
    CODE_200: '200',
    CODE_404: '404',
    CODE_400: '400',
    CODE_500: '500'
}