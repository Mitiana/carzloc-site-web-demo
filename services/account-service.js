import { WebServiceException } from "../exceptions/webservice-exception"

export async function validateAccount(param) {
    try {
        const url = `${process.env.API_HOST}/account/validate`
        const request = await fetch(url, { method: 'POST', body: param })
        const response = await request.json()
        return response
    } catch {
        throw new WebServiceException(`Une erreur s'est produite lors de la validation du compte`)
    }
}