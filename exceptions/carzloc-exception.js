import { ExceptionTypes } from "../enums/exception-types";

export class CarzlocException {
    constructor(message) {
        this.message = message;
        this.name = ExceptionTypes.CARZLOC_EXCEPTION;
    }
}