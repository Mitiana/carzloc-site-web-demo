import { ExceptionTypes } from "../enums/exception-types";

export class WebServiceException {
    constructor(message) {
        this.message = message;
        this.name = ExceptionTypes.WEB_SERVICE_EXCEPTION;
    }
}