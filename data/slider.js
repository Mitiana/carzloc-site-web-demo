import slide1 from '../public/images/slide/slide-1.webp'
import slide2 from '../public/images/slide/slide-2.webp'
import slide3 from '../public/images/slide/slide-3.webp'

const slideData = [
	{
		title: ('Ouverture'),
		description: <span>229 Rue Emile Mengin 45200 MONTARGIS</span>,
		button: 'Nous contacter',
		image: slide1.src,
		url: '/contact'
	},
	{
		title: ('199€/jour'),
		description: (<span>BMW 520D G30 <br/> à partir de</span>),
		button: 'Réserver',
		image: slide2.src,
		url: '/nos-vehicules'
	},
	{
		title: ('29€/jour'),
		description: <span>NOS CITADINES <br/>à partir de</span>,
		button: 'Voir nos véhicules',
		image: slide3.src,
		url: '/nos-vehicules'
	}
];

export default slideData