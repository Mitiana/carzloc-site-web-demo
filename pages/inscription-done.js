import { useRouter } from "next/router";
import inscriptionStyle from '../styles/components/InscriptionDone.module.scss'
import OptimizedImage from '../components/OptimizedImage'

export default function InscriptionDone() {
    const router = useRouter()
    const emailIcon = require('../public/images/email.webp')
    return (
        <div className={inscriptionStyle.InscriptionDoneContainer}>
            <div className={inscriptionStyle.InscriptionDone}>
                <OptimizedImage src={emailIcon} width={128} height={128} alt={'Email icon'} />
                <h1>Votre inscription est bien enregistrée</h1>
                <p>Un e-mail automatique de confirmation vient d&apos;être envoyé à l&apos;adresse: <strong className="text-primary">{router.query.email}</strong>. Cliquer le lien fourni dans l&apos;e-mail pour terminer votre inscription.</p>
                <strong>Merci pour votre confiance!</strong>
                <div className={inscriptionStyle.separator}></div>
            </div>
        </div>
    )
}