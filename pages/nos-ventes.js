import PageHeader from '../components/PageHeader';
import SectionTitle from '../components/SectionTitle';
import OptimizedImage from '../components/OptimizedImage';
import Link from 'next/link'
import salesStyle from '../styles/components/NosVentes.module.scss'

function NosVentes() {
    return (
        <>
            <PageHeader title="Nos ventes" backgroundImageUrl={'/images/nos-ventes.webp'}/>
            <div className="container">
                <SectionTitle>CAR<span>Z</span>LOC POSSÈDE UN SERVICE DESTINÉ À LA VENTE DE VÉHICULES D’OCCASION</SectionTitle>
                <div className={salesStyle.ourSales}>
                    <div className={salesStyle.ourSalesDescription}>
                        <h3>Vous souhaitez changer de voiture ?</h3>
                        <p>
                            Vos agences Carzloc installées à Nemours et à Montargis ont le plaisir de vous accueillir et de vous recevoir pour discuter de vos futurs projets automobiles. À la recherche des bonnes affaires ? Notre service de vente de véhicules d’occasion toutes marques vous accompagne selon vos envies. Un large choix de véhicules d’exception à la vente vous est proposé :
                        </p>
                        <ul>
                            <li>Compact</li>
                            <li>Citadine</li>
                            <li>Sportive</li>
                            <li>Berline</li>
                            <li>S.U.V</li>
                            <li>Break ou Familiale</li>
                            <li>Haut de gamme</li>
                        </ul>
                        <p>
                            Nos véhicules présentés sont garanties et disponibles à l&pos;essai. Notre qualité d’accueil, notre professionnalisme, notre attachement à la qualité, ainsi que la relation que nous entretenons avec nos clients sont le résultat de nombreuses années d’expérience dans la vente de véhicules d’occasion.
                        </p>
                        <Link href={'/contact'}>
                            <a>
                                Contactez-nous pour plus d&apos;informations
                            </a>
                        </Link>
                    </div>
                    <div className={salesStyle.ourSalesImage}>
                        <OptimizedImage src={require('../public/images/carzloc-metier.webp')}
                                        alt="Métier de CARZLOC"
                                        width={550}
                                        height={379}/>
                    </div>
                </div>
            </div>
        </>
    )
}

export default NosVentes