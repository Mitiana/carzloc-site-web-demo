import { useRouter } from 'next/router';
import Link from 'next/link'
import { useState, useEffect } from "react";
import inscriptionStyle from '../styles/components/InscriptionDone.module.scss'
import ReactLoading from 'react-loading'
import Alert from '../components/utils/Alert'
import OptimizedImage from '../components/OptimizedImage'
import { CarzlocException } from '../exceptions/carzloc-exception';
import { validateAccount } from '../services/account-service'
import { ExceptionTypes } from '../enums/exception-types';
import { ReturnCode } from '../enums/return-code';
import { WebServiceException } from '../exceptions/webservice-exception';

export default function InscriptionValidation(pageProps) {
    const { setShowLogin, loggedIn, query } = pageProps
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(null)
    const [validToken, setValidToken] = useState(false)
    const router = useRouter()

    useEffect(() => {
        async function onInit() {
            const encodedToken = JSON.parse(query).token
            try {
                if (!encodedToken) throw new CarzlocException('Pas de token trouvé')
                const decodedToken = atob(encodedToken)
                const token = decodedToken.split('|')
                setValidToken(true)
                const id = Number(token[0])
                const reference = token[1]
                const param = JSON.stringify({ id, reference })
                const jsonResponse = await validateAccount(param)

                if (jsonResponse.code !== ReturnCode.CODE_200) {
                    if (jsonResponse.message) throw new WebServiceException(jsonResponse.message)
                    throw new WebServiceException(`Une erreur s'est produite lors de la validation du compte`)
                }

                setLoading(false)
                setError(null)
            } catch (e) {
                setError('test')
                handleException(e, router, setError, setLoading)
            }

        }
        onInit()
    }, [setError, setLoading, router, setValidToken, query])

    if(loggedIn) {
        router.push('/compte')
        return null
    }

    if (!validToken) return null

    if (error && !loading) return <ErrorDOM error={error} />

    if (!error && !loading) return <SuccessDOM setShowLogin={setShowLogin} loggedIn={loggedIn} />

    return (
        <div className={inscriptionStyle.InscriptionDoneContainer}>
            <div className={inscriptionStyle.InscriptionDone}>
                {loading && <ReactLoading type={'spin'} color="#1e729f" width={50} height={50} />}
                {!error && !loading && <OptimizedImage src={doneIcon} alt="Validation compte effectuée" width={50} height={50} />}
                <h1>Validation de compte en cours</h1>
                <p>Votre compte est en cours de validation, merci de patienter.</p>
                <strong>Merci pour votre votre confiance!</strong>
                <div className={inscriptionStyle.separator}></div>
                {!loggedIn && !loading && !error && <button className="btn btn-primary" onClick={() => setShowLogin(true)}>Se connecter</button>}
                {loggedIn && !loading && !error &&
                    <Link href="/compte">
                        <a className="btn btn-primary">Mon compte</a>
                    </Link>
                }
            </div>
        </div>
    )
}

function SuccessDOM({ setShowLogin, loggedIn }) {
    const doneIcon = require('../public/images/icon/done.webp')
    return (
        <div className={inscriptionStyle.InscriptionDoneContainer}>
            <div className={inscriptionStyle.InscriptionDone}>
                <OptimizedImage src={doneIcon} alt="Activation compte effectuée" width={50} height={50} />
                <h1>Compte activé avec succès</h1>
                <Alert type="success" className="text-center">Votre compte est activé, vous pouvez maintenant connecter à votre compte an cliquant le bouton ci-dessous.</Alert>
                <div className={inscriptionStyle.separator}></div>
                {!loggedIn && <button className="btn btn-primary" onClick={() => setShowLogin(true)}>Se connecter</button>}
                {loggedIn &&
                    < Link href="/compte">
                        <a className="btn btn-primary">Mon compte</a>
                    </Link>
                }
            </div>
        </div >
    )
}

function ErrorDOM({ error }) {
    const errorIcon = require('../public/images/icon/error.webp')
    return (
        <div className={inscriptionStyle.InscriptionDoneContainer}>
            <div className={inscriptionStyle.InscriptionDone}>
                <OptimizedImage src={errorIcon} alt="Erreur lors de la validation du compte" width={50} height={50} />
                <h1>{error}</h1>
                <Alert className="text-center">Une erreur s&apos;est produite durant la validation de votre compte, merci de réessayer ultérieurement.</Alert>
                <div className={inscriptionStyle.separator}></div>
                <Link href="/">
                    <a className="text-primary"><i className="fas fa-long-arrow-alt-left" /> Revenir à l&apos;accueil</a>
                </Link>
            </div>
        </div>
    )
}

function handleException(e, router, setError, setLoading) {
    console.log('e', e)
    switch (e.name) {
        case ExceptionTypes.WEB_SERVICE_EXCEPTION:
            setLoading(false)
            setError(e.message)
            break;
        default:
            router.push('/404')
            break;
    }
}

export async function getServerSideProps({ query }) {
    return {
        props: {
            query: JSON.stringify(query)
        }
    }
}