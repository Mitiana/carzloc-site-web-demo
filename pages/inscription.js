import PageHeader from '../components/PageHeader';
import inscriptionStyle from '../styles/components/Inscription.module.scss'
import { useEffect, useState } from 'react';
import { useForm } from "react-hook-form";
import { format } from 'date-fns'
import FieldFormAlert from '../components/utils/FieldFormAlert';
import Alert from '../components/utils/Alert';
import { useRouter } from 'next/router'
const COMPANY_TYPE_ID = "15"
const GLOBAL_ERROR_MESSAGE = `Une erreur s'est produite durant l'opération, merci de réessayer ultérieurement`

export default function Inscription() {
    const [reference, setReference] = useState(null)
    const { register, formState: { errors }, handleSubmit, setValue, watch } = useForm();
    const todayDate = format(new Date(), 'yyyy-MM-dd')
    const [emailError, setEmailError] = useState(null)
    const [globalError, setGlobalError] = useState(null)
    const router = useRouter()

    useEffect(() => {
        async function getNextReference() {
            const url = `${process.env.API_HOST}/customer/next-reference`
            try {
                const request = await fetch(url)
                const jsonResponse = await request.json()
                const reference = jsonResponse.data
                setReference(reference)
                setValue('reference', reference)
                setValue('customerTypeId', "14")
            } catch (e) {
                console.log(`Erreur lors de la récupération de la liste des véhicules, URL ${url}`, e)
            }
        }
        getNextReference()
    }, [setReference, setValue])

    const onSubmit = async (values) => {
        const param = {
            customerTypeId: Number(values.customerTypeId),
            name: values.name,
            firstName: values.firstName,
            reference: values.reference,
            cin: values.cni,
            dateOfBirth: values.dateOfBirth,
            placeOfBirth: values.placeOfBirth,
            phone1: values.phone1,
            phone2: values.phone2,
            email: values.email,
            city: values.city,
            adress: values.adress,
            postalCode: values.postalCode,
            permitNumber: values.permitNumber,
            permitDateIssue: values.permitDateIssue,
            permitPlaceIssue: values.permitPlaceIssue,
            securityNumber: values.securityNumber,
            siret: values.siret,
            billingAddress: values.billingAddress,
            fromWebsite: true
        }
        if (param.customerTypeId.toString() !== COMPANY_TYPE_ID) {
            delete param.securityNumber
            delete param.siret
            delete param.billingAddress
        }
        try {
            const url = `${process.env.API_HOST}/customer`
            const request = await fetch(url, { method: 'POST', body: JSON.stringify(param) })
            const response = await request.json()
            if (response.code === '200') {
                router.push({
                    pathname: '/inscription-done',
                    query: {email: param.email}
                })
            } else {
                if(response.message) {
                    setEmailError(response.message)
                } else {
                    setGlobalError(GLOBAL_ERROR_MESSAGE)
                }
            }
            console.log('response', response)
        } catch {
            setGlobalError(GLOBAL_ERROR_MESSAGE)
        }
    }

    const isCompany = watch('customerTypeId')

    return (
        <>
            <PageHeader title="INSCRIPTION" backgroundImageUrl="/images/contact.webp" />
            <div className={inscriptionStyle.inscription}>
                <div className="container">
                    <p className="text-center">Merci de remplir le formulaire ci-dessous. Les champs marqués par <span className="text-danger">*</span> sont obligatoires.</p>
                    {globalError && <Alert className="text-center">{globalError}</Alert>}
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className={['row', inscriptionStyle.customerTypeContainer].join(' ')}>
                            <input type="radio"
                                name="customerTypeId"
                                id="particularType"
                                value="14"
                                {...register('customerTypeId', { required: true })} />
                            <label htmlFor="particularType">Particulier</label>

                            <input type="radio"
                                name="customerTypeId"
                                id="companyType"
                                value="15"
                                {...register('customerTypeId', { required: true })} />
                            <label htmlFor="companyType">Société</label>
                        </div>
                        <div className="row">
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="name">Nom <span className="text-danger">*</span> :</label>
                                    <input type="text"
                                        id="name"
                                        className="form-control"
                                        name="name"
                                        {...register('name', { required: true })} />
                                </div>
                            </div>
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="firstName">Prénom <span className="text-danger">*</span> :</label>
                                    <input type="text"
                                        id="firstName"
                                        className="form-control"
                                        name="firstName"
                                        {...register('firstName', { required: true })} />
                                </div>
                            </div>
                            <div className="column">
                                <div className="form-group d-none">
                                    <label htmlFor="reference">Référence <span className="text-danger">*</span> :</label>
                                    <input type="text"
                                        id="reference"
                                        className="form-control"
                                        name="reference"
                                        readOnly
                                        {...register('reference', { required: true })} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="cni">CNI <span className="text-danger">*</span> :</label>
                                    <input type="text"
                                        id="cni"
                                        className="form-control"
                                        name="cni"
                                        {...register('cni', { required: true })} />
                                </div>
                            </div>
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="dateOfBirth">Date de naissance <span className="text-danger">*</span> :</label>
                                    <input type="date"
                                        id="dateOfBirth"
                                        className="form-control"
                                        name="dateOfBirth"
                                        max={todayDate}
                                        {...register('dateOfBirth', { required: true })} />
                                </div>
                            </div>
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="placeOfBirth">Lieu de naissance <span className="text-danger">*</span> :</label>
                                    <input type="text"
                                        id="placeOfBirth"
                                        className="form-control"
                                        name="placeOfBirth"
                                        {...register('placeOfBirth', { required: true })} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="phone1">Téléphone 1 <span className="text-danger">*</span> :</label>
                                    <input type="text"
                                        id="phone1"
                                        className="form-control"
                                        name="phone1"
                                        {...register('phone1', { required: true })} />
                                </div>
                            </div>
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="phone2">Téléphone 2 :</label>
                                    <input type="text"
                                        id="phone2"
                                        className="form-control"
                                        name="phone2"
                                        {...register('phone2')} />
                                </div>
                            </div>
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="email">Email <span className="text-danger">*</span> :</label>
                                    <input type="email"
                                        id="email"
                                        className="form-control"
                                        name="email"
                                        onInput={() => setEmailError(null)}
                                        {...register('email', { required: true })}
                                    />
                                    <FieldFormAlert content={(<><i className="fas fa-exclamation-triangle"/>{emailError}</>)}
                                        condition={emailError} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="city">Ville <span className="text-danger">*</span> :</label>
                                    <input type="text"
                                        id="city"
                                        className="form-control"
                                        name="city"
                                        {...register('city', { required: true })} />
                                </div>
                            </div>
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="adress">Adresse <span className="text-danger">*</span> :</label>
                                    <input type="text"
                                        id="adress"
                                        className="form-control"
                                        name="adress"
                                        {...register('adress', { required: true })} />
                                </div>
                            </div>
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="postalCode">Code postal <span className="text-danger">*</span> :</label>
                                    <input type="text"
                                        id="postalCode"
                                        className="form-control"
                                        name="postalCode"
                                        {...register('postalCode', { required: true })} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="permitNumber">Numéro permis <span className="text-danger">*</span> :</label>
                                    <input type="text"
                                        id="permitNumber"
                                        className="form-control"
                                        name="permitNumber"
                                        {...register('permitNumber', { required: true })} />
                                </div>
                            </div>
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="permitDateIssue">Date de délivrance <span className="text-danger">*</span> :</label>
                                    <input type="date"
                                        id="permitDateIssue"
                                        className="form-control"
                                        name="permitDateIssue"
                                        max={todayDate}
                                        {...register('permitDateIssue', { required: true })} />
                                </div>
                            </div>
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="permitPlaceIssue">Lieu de délivrance <span className="text-danger">*</span> :</label>
                                    <input type="text"
                                        id="permitPlaceIssue"
                                        className="form-control"
                                        name="permitPlaceIssue"
                                        {...register('permitPlaceIssue', { required: true })} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="securityNumber">Numéro de caution :</label>
                                    <input type="text"
                                        id="securityNumber"
                                        className="form-control"
                                        name="securityNumber"
                                        {...register('securityNumber')} />
                                </div>
                            </div>
                            <div className="column">
                                <div className="form-group">
                                    <label htmlFor="expirationDate">Date d&apos;expiration :</label>
                                    <input type="date"
                                        id="expirationDate"
                                        className="form-control"
                                        name="expirationDate"
                                        min={todayDate}
                                        {...register('expirationDate')} />
                                </div>
                            </div>
                            <div className="column"></div>
                        </div>
                        {
                            isCompany === COMPANY_TYPE_ID &&
                            <div className="row">
                                <div className="column">
                                    <div className="form-group">
                                        <label htmlFor="company">Raison sociale <span className="text-danger">*</span> :</label>
                                        <input type="text"
                                            id="company"
                                            className="form-control"
                                            name="company"
                                            {...register('company', { required: true })} />
                                    </div>
                                </div>
                                <div className="column">
                                    <div className="form-group">
                                        <label htmlFor="siret">Siret <span className="text-danger">*</span> :</label>
                                        <input type="text"
                                            id="siret"
                                            className="form-control"
                                            name="siret"
                                            {...register('siret', { required: true })} />
                                    </div>
                                </div>
                                <div className="column">
                                    <div className="form-group">
                                        <label htmlFor="billingAddress">Adresse de facturation :</label>
                                        <input type="text"
                                            id="billingAddress"
                                            className="form-control"
                                            name="billingAddress"
                                            {...register('billingAddress')} />
                                    </div>
                                </div>
                            </div>
                        }
                        <div className="text-center">
                            <button className="btn btn-primary">S&apos;inscrire</button>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}