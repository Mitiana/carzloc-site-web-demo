import Link from 'next/link'
import vehicleStyle from '../../../styles/components/Vehicle.module.scss'
import Booking from '../../../components/vehicle/Booking'
import BookingSummary from '../../../components/vehicle/BookingSummary'
import CurrencyValue from '../../../components/utils/CurrencyValue';
import { useState, useEffect } from 'react'
import dateDiff from '../../../components/utils/DateHelper'
import Alert from '../../../components/utils/Alert'
import SaveBooking from '../../../components/vehicle/SaveBooking';
import CustomNotFound from '../../404';
const DAILY_PRICE_ID = 12
const TOKEN_KEY = 'CARZLOC_TOKEN'

function Vehicle({ car, loggedIn, showLogin, setShowLogin }) {
    const [startDate, setStartDate] = useState(null)
    const [endDate, setEndDate] = useState(null)
    const [price, setPrice] = useState(null)
    const [errorMessage, setErrorMessage] = useState(null)
    const [options, setOptions] = useState([])

    if (!car) {
        return (<CustomNotFound/>)
    }

    return (
        <div className="container">
            <div className={vehicleStyle.vehicle}>
                <div className={vehicleStyle.carLeft}>
                    <CarInformations car={car} />
                    <Alert className={vehicleStyle.alertMessage}>{errorMessage}</Alert>
                    <Booking car={car}
                        loggedIn={loggedIn}
                        setShowLogin={setShowLogin}
                        startDate={startDate}
                        setStartDate={setStartDate}
                        endDate={endDate}
                        setEndDate={setEndDate}
                        setErrorMessage={setErrorMessage}
                        setPrice={setPrice}
                        onCheckAvailabilityCallback={() => handleAvailability(startDate, endDate, car, setPrice, setErrorMessage)} />
                    <BookingSummary price={price}
                        beginDate={startDate}
                        endDate={endDate}
                        options={options}
                    />
                    <SaveBooking loggedIn={loggedIn} price={price} />
                </div>
                <CarAbstract car={car}
                    options={options}
                    setOptions={setOptions}
                    price={price}
                />
            </div>
        </div>
    )
}

function handleAvailability(startDate, endDate, car, setPrice, setErrorMessage) {
    setPrice(null)
    const daysNumber = dateDiff(startDate, endDate)
    const prices = car.prices.filter(price => price.priceType.id === DAILY_PRICE_ID)
    const selectedPrice = prices.find(price => price.minDay <= daysNumber && daysNumber <= price.maxDay)
    if (selectedPrice) {
        setPrice(selectedPrice)
    } else {
        setErrorMessage(
            <>
                Aucun tarif disponible pour une résérvation de {daysNumber} jours,
                <Link href={'/contact'}>
                    <a>
                        <strong style={{ color: '#155724' }}>
                            &nbsp; Demander un devis <i className="fas fa-long-arrow-alt-right" />
                        </strong>
                    </a>
                </Link>
            </>
        )
    }
}


function CarInformations({ car }) {
    return (
        <div className={vehicleStyle.carInformations}>
            <div>
                {/* eslint-disable-next-line @next/next/no-img-element */}
                <img src={car.featuredImage.data}
                    alt={car.name}
                    width={700} />
            </div>
            <div className={vehicleStyle.carDescription}>
                {car.description}
            </div>
        </div>
    )
}

function CarAbstract({ car, options, setOptions, price }) {
    const categoriesDOM = getCategoriesDOM(car.categories);
    const attributesDOM = getAttributesDOM(car);
    const pricesDOM = getPricesDOM(car.prices);
    const optionsDOM = getOptionsDOM(car.options, options, setOptions);
    return (
        <div className={vehicleStyle.carAbstract}>
            <h1 className={vehicleStyle.carName}>{car.name}</h1>
            <div className={vehicleStyle.carExcerpt}>
                <p>{car.excerpt}</p>
            </div>
            <div className={vehicleStyle.carCategories}>
                {categoriesDOM}
            </div>
            <div className={vehicleStyle.carBail}>
                Caution: <CurrencyValue value={car.bail} />
            </div>
            <div className={[vehicleStyle.carCharacteristics, vehicleStyle.lightBackground].join(' ')}>
                <h2>Caractéristiques</h2>
                <ul className={vehicleStyle.attributes}>
                    {attributesDOM}
                </ul>
            </div>
            <div className={vehicleStyle.lightBackground}>
                <h2>Tarifs</h2>
                <div>
                    {pricesDOM}
                </div>
            </div>
            <Options optionsDOM={optionsDOM} price={price} />
        </div>
    )
}

/** CATEGORIES */
function getCategoriesDOM(categories) {
    const categoriesDOM = [];
    for (const category of categories) {
        categoriesDOM.push((
            <CategoryLink category={category} key={category.id} />
        ));
    }
    return categoriesDOM;
}

function CategoryLink({ category }) {
    return (
        <Link href={`/categorie/${category.id}`}>
            <a>
                <i className="fa fa-tag" /> {category.name}
            </a>
        </Link>
    )
}

/** ATTRIBUTES */
function getAttributesDOM(car) {
    const attributesDOM = [];
    attributesDOM.push(<Attribute icon="stm-icon-ac-max-passenger" name="Places" value={car.place} key="1" />)
    attributesDOM.push(<Attribute icon="stm-icon-engine" name="Puissance" value={car.power} key="2" />)
    attributesDOM.push(<Attribute icon="stm-icon-transmission" name="Boite" value={car.gearbox} key="3" />)
    attributesDOM.push(<Attribute icon="stm-icon-car_sale" name="Caution" value={<CurrencyValue value={car.bail} />} key="4" />)
    attributesDOM.push(<Attribute icon="stm-icon-fuel" name="Carburant" value={car.fuel} key="5" />)
    return attributesDOM;
}

function Attribute({ icon, name, value }) {
    return (
        <li>
            <span><i className={icon} /></span>
            <span>{name} :</span>
            <span className={vehicleStyle.attributeValue}>{value}</span>
        </li>
    )
}

/** PRICES */
function getPricesDOM(prices) {
    const pricesDOM = [];
    for (const price of prices) {
        pricesDOM.push(<Price price={price} key={price.id} />)
    }
    return pricesDOM;
}

function Price({ price }) {
    const priceLabel = getPriceLabel(price);
    return (
        <div className={vehicleStyle.price}>
            <div>{price.label} ({price.kilometer} km inclus)</div>
            <div>{priceLabel}</div>
        </div>
    )
}

function getPriceLabel(price) {
    const SINGLE_PRICE_ID = 13;
    if (price.priceType.id === SINGLE_PRICE_ID) {
        return (
            <Link href={'/contact'}>
                <a>
                    Sur devis <i className="fas fa-long-arrow-alt-right" />
                </a>
            </Link>
        )
    }
    return <CurrencyValue value={price.price} suffix=" par jour" />
}

/** OPTIONS */
function Options({ optionsDOM, price }) {
    if (!price) {
        return null
    }
    return (
        <div className={vehicleStyle.lightBackground}>
            <h2>Options</h2>
            <div className={vehicleStyle.options}>
                {optionsDOM}
            </div>
        </div>
    )
}

function getOptionsDOM(carOptions, options, setOptions) {
    const optionsDOM = [];
    for (const option of carOptions) {
        optionsDOM.push(
            <Option option={option}
                key={option.id}
                options={options}
                setOptions={setOptions} />
        )
    }
    return optionsDOM;
}

function Option({ option, options, setOptions }) {
    return (
        <div className={vehicleStyle.option}>
            <div>
                <div>
                    <label className="switch">
                        <input type="checkbox"
                            id={option.id}
                            onChange={(value) => {
                                if (value.target.checked) {
                                    const selectedOptions = [...options]
                                    selectedOptions.push(option)
                                    setOptions(selectedOptions)
                                } else {
                                    const selectedOptions = options.filter(o => o.id !== option.id)
                                    setOptions(selectedOptions)
                                }
                            }}
                        />
                        <span className="switch-content" />
                    </label>
                </div>
            </div>
            <div className={vehicleStyle.optionLabel}>
                <label htmlFor={option.id}><strong>{option.label}</strong> ({option.priceType.label})</label>
            </div>
            <div className={vehicleStyle.optionPrice}>
                <CurrencyValue value={option.price} />
            </div>
        </div>
    )
}

export async function getStaticProps({ params }) {
    const { slug, id } = params
    const url = `${process.env.API_HOST}/front/car/${id}/${slug}`
    try {
        const request = await fetch(url)
        const jsonResponse = await request.json()
        const car = jsonResponse.data
        return {
            props: {
                car
            },
            revalidate: 60
        }
    } catch {
        return {
            props: {
                car: null
            },
            revalidate: 10
        }
    }
}

export async function getStaticPaths() {
    const url = `${process.env.API_HOST}/front/car/paths`
    try {
        const request = await fetch(url)
        const response = await request.json()
        const carsIds = response.data
        return {
            paths: carsIds.map(path => ({
                params: { id: path.id.toString(), slug: path.slug }
            })),
            fallback: true
        }
    } catch {
        return {
            paths: [],
            fallback: true
        }
    }
}

export default Vehicle
