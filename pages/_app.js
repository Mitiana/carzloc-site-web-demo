import '../styles/style.scss'
import 'react-datepicker/dist/react-datepicker.min.css'
import '@fontsource/open-sans/400.css'
import '@fontsource/open-sans/600.css'
import '@fontsource/open-sans/700.css'
import '@fontsource/open-sans/800.css'
import '@fontsource/montserrat/400.css'
import '@fontsource/montserrat/700.css'
import '@fontsource/ropa-sans/400.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import '../public/fonts/stm-icon/stm-icon.css'
import Layout from '../components/Layout'
import Router from 'next/router'
import NProgress from 'nprogress'
import { registerLocale } from 'react-datepicker'
import fr from 'date-fns/locale/fr'
import { useEffect, useState } from 'react'
import Modal from '../components/utils/Modal'
import Login from '../components/Login'
registerLocale("fr", fr)
const TOKEN_KEY = 'CARZLOC_TOKEN'

function MyApp({ Component, pageProps }) {
    const [loggedIn, setLoggedIn] = useState(false)
    const [showLogin, setShowLogin] = useState(false)

    useEffect(() => {
        const token = localStorage.getItem(TOKEN_KEY)
        if (token) {
            const expiredToken = new Date(parseInt(atob(token), 10));
            const currentDate = new Date();
            if (expiredToken < currentDate) {
                localStorage.removeItem(TOKEN_KEY)
            } else {
                setLoggedIn(true)
            }
        }
    }, [setLoggedIn])

    pageProps.loggedIn = loggedIn
    pageProps.setLoggedIn = setLoggedIn

    pageProps.showLogin = showLogin
    pageProps.setShowLogin = setShowLogin

    Router.events.on('routeChangeStart', () => {
        setShowLogin(false)
        NProgress.start()
    })

    Router.events.on('routeChangeComplete', () => {
        NProgress.done()
    })

    return (
        <Layout {...pageProps}>
            <Component {...pageProps} />
            <Modal showModal={showLogin}
                setShowModal={setShowLogin}
                setLoggedIn={setLoggedIn}
                template={<Login setShowLogin={pageProps.setShowLogin} setLoggedIn={setLoggedIn} />}
            />
        </Layout>
    )
}

export default MyApp
