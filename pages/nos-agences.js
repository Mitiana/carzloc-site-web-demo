import PageHeader from '../components/PageHeader';
import SectionTitle from '../components/SectionTitle';
import TextLogo from '../components/TextLogo';
import Link from 'next/link'
import agenceStyle from '../styles/components/NosAgence.module.scss'
import OptimizedImage from '../components/OptimizedImage';

export default NosAgences

function NosAgences() {

    return (
        <>
            <PageHeader title="Nos agences"/>
            <AgenceInformations/>
            <RentalOptions/>
            <CarzlocChoice/>
        </>
    )
}

function AgenceInformations() {
    return (
        <div className="container">
            <SectionTitle isBigSpan={true}>Société Car<span>Z</span>loc</SectionTitle>
            <div className={agenceStyle.agenceInformations}>
                <div className={agenceStyle.agenceHistory}>
                    <h2>Notre histoire</h2>
                    <p>
                        Notre société <TextLogo/> a été créée en 2018. <TextLogo/> c’est 2 agences de location situées à
                        Nemours (77) et Montargis (45). Nous sommes spécialisées dans la location de voitures et
                        d’utilitaires dédiées aux particuliers et aux professionnels. Nos agences de location mettent à
                        votre disposition un parc de véhicules variés au meilleur prix pour satisfaire vos attentes en
                        matière de mobilité : De la petite citadine entièrement climatisée, à la compact, en passant par
                        la Berline, au SUV, sans oublier les familiales et nos voitures de luxe pour couvrir vos
                        évènements VIP. <TextLogo/> c’est aussi un vaste choix de véhicules utilitaires pour répondre à
                        vos besoins professionnels ou privés.
                    </p>
                </div>
                <div className={agenceStyle.agenceNemours}>
                    <h2>Agence CarZloc de Nemours</h2>
                    <ul>
                        <li>29, rue des Tanneurs 77140 Nemours</li>
                        <li>
                            <a href="tel:+33(0)164788486">
                                33 (0) 1 64 78 84 86
                            </a>
                        </li>
                        <li>
                            <a href="mailto:contact@carzloc.fr">
                                contact@carzloc.fr
                            </a>
                        </li>
                    </ul>
                </div>
                <div className={agenceStyle.agenceMontargis}>
                    <h2>Agence CarZloc de Montargis</h2>
                    <ul>
                        <li>229 rue Emile Mengin 45200</li>
                        <li>
                            <a href="tel:+33(0)761934846">
                                33 (0) 7 61 93 48 46
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

function RentalOptions() {
    const rentalImageUrl = '/images/rental-options.webp'
    return (
        <div className={agenceStyle.rentalOptions}>
            <div className={agenceStyle.rentalOptionsImage}>
                {/* eslint-disable-next-line @next/next/no-img-element */}
                <img src={rentalImageUrl} alt="CARZLOC - Options de location"/>
            </div>
            <div className={agenceStyle.rentalOptionsDescription}>
                <h3>Quelles options de locations sont disponibles chez nous ?</h3>
                <p>
                    CarZloc offre une flotte de véhicules neufs de grandes marques tels que BMW, Mercedes, Ford Mustang,
                    Peugeot, Renault, Volkswagen, Toyota,... Notre gamme de voitures est déjà classée par catégorie.
                    Retrouvez la classe « prestige, luxe & V.I.P » pour vos week-end, voyages d&apos;affaires ou par pur
                    plaisir. Les « citadines » à la fois compactes et récentes mais surtout très économiques et qui
                    offrent de multiples options. Ces voitures vous fourniront un sentiment de liberté, de confort et
                    d’élégance. Tous nos véhicules peuvent aussi être louées pour des escapades d&apos;un week-end ou
                    bien pour des vacances.
                </p>
                <p>
                    Nous proposons également à la location des voitures familiales, des SUV et berlines aux durées
                    personnalisables : LCD (location courte durée) ou LLD (location longue durée).
                </p>
                <p>
                    Découvrez aussi nos voitures utilitaires dédiées aux professionnels ou particuliers pour assurer vos
                    déménagements privés. Avec un parc de camions et fourgons pour vos utilisations ponctuelles ou de
                    longues durées. Elles peuvent vous conduire dans la banlieue parisienne.
                </p>
            </div>
        </div>
    )
}

function CarzlocChoice() {
    return (
        <div className="container">
            <SectionTitle isBigSpan={true}>POURQUOI CHOISIR CAR<span>Z</span>LOC ?</SectionTitle>
            <div className={agenceStyle.carzlocChoice}>
                <div className={agenceStyle.carzlocChoiceDescription}>
                    <p>
                        Avec les agences <TextLogo/>, vous pouvez louer une voiture avec ou sans chauffeur selon vos préférences et vos besoins. Sachez que nos chauffeurs sont tous très compétents et très expérimentés dans leurs domaines. Notre société vous offre les meilleures prestations à thème au meilleur rapport qualité prix. Ainsi, nous vous accompagnons sur tous les trajets grâce à une sélection de véhicules de tourisme disponibles. Notre service est destiné à tous, peu importe si vous êtes un professionnels ou un particulier. Nos nombreuses voitures sont toutes en très bon état. Elles sont très bien entretenues et font toujours l’objet de contrôles méticuleux pour garantir la sérénité de vos trajets. Montez à bord d’un véhicule élégant, confortable et puissant pour assurer vos déplacements.
                    </p>
                    <p>
                        Nous vous offrons diverses marques de voiture selon votre motif de location. Comme voiture de prestige, vous pouvez louer une Mercedes Classe G ou une Ford Mustang coupé. Le luxe vous donne rendez-vous au volant d’une BMW de luxe. Tandis que l’option citadine comporte la Peugeot 208, la Renault Capture II ou encore la célèbre Fiat 500. Réservez votre véhicule 7j/7 en quelques clics. Quelques soit votre demande nous vous proposons un véhicule qui vous correspond ! <TextLogo/> vous accompagne sur la route de vos trajets. Ainsi pour satisfaire votre besoin de location de voiture, n&apos;hésitez pas à faire appel à <TextLogo/>, un professionnel de la location de véhicules & d&apos;utilitaires.
                    </p>
                </div>
                <div className={agenceStyle.carzlocChoiceImage}>
                    <OptimizedImage
                        src={require('../public/images/carzloc-choice.webp')}
                        width={600}
                        height={317}
                        alt="CARZLOC - Voiture bleu"/>
                    <ul>
                        {categories.map((category, key) => (
                            <Category category={category}
                                      key={key}/>
                        ))}
                    </ul>
                </div>
            </div>
        </div>
    )
}

function Category({category}) {
    return (
        <li>
            <Link href={category.link}>
                <a>{category.label}</a>
            </Link>
        </li>
    )
}

const categories = [
    {
        label: 'Louer une voiture prestige',
        link: '/categories/berline'
    },
    {
        label: 'Voiture luxe',
        link: '/categories/luxe'
    },
    {
        label: 'Voiture citadine',
        link: '/categories/citadine'
    },
    {
        label: 'Voiture familiale',
        link: '/categories/familiale'
    },
    {
        label: 'Voiture compacte',
        link: '/categories/compact'
    },

    {
        label: 'Voiture utilitaire',
        link: '/categories/utilitaire'
    }
]