import notFoundStyle from '../styles/components/NotFound.module.scss'
import Link from 'next/link'

function CustomNotFound({label, code}) {
    return (
        <div className="container">
            <div className={notFoundStyle.notFoundContainer}>
                <i className={['far fa-frown', notFoundStyle.notFoundIcon].join(' ')}/>
                {code && (
                    <span className={notFoundStyle.notFoundCode}>404</span>
                )}
                <h2 className={notFoundStyle.notFoundLabel}>{label}</h2>
                <Link href="/">
                    <a className={notFoundStyle.linkToHome}>
                        &#10229; Revenir à la page d&apos;accueil
                    </a>
                </Link>
            </div>
        </div>
    )
}

CustomNotFound.defaultProps = {
    label: 'Page introuvable',
    code: '404'
}

export default CustomNotFound
