import PageHeader from '../components/PageHeader';
import vehiclesStyle from '../styles/components/NosVehicules.module.scss'
import ArchiveSidebar from '../components/ArchiveSidebar';
import ArchiveContent from '../components/ArchiveContent';

function NosVehicules({cars}) {
    return (
        <>
            <PageHeader title="Nos véhicules" backgroundImageUrl="/images/nos-vehicules.webp" />
            <div className={vehiclesStyle.vehicles}>
                <div className="container">
                    <div className={vehiclesStyle.vehiclesContainer}>
                        <ArchiveSidebar />
                        <ArchiveContent cars={cars}/>
                    </div>
                </div>
            </div>
        </>
    )
}

export async function getStaticProps() {
    try {
        const url = `${process.env.API_HOST}/front/car`
        const request = await fetch(url)
        const response = await request.json()
        return {
            props: {
                cars: response.data
            },
            revalidate: 60,
        }
    } catch {
        return {
            props: {
                cars: []
            },
            revalidate: 10,
        }
    }
}

export default NosVehicules

