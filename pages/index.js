import homeStyle from '../styles/components/Home.module.scss'
import Link from 'next/link'
import SectionTitle from '../components/SectionTitle';
import OptimizedImage from '../components/OptimizedImage';
import HomeSlider from '../components/HomeSlider';

function Index() {

    return (
        <>
            <Slider />
            <Speciality />
            <Categories />
        </>
    )
}

function Slider() {
    const imageUrl = '/images/voitures-categorie-citadine-carzloc.webp'
    return <HomeSlider />
}

function Speciality() {
    return (
        <div className={homeStyle.speciality}>
            <div className={homeStyle.specialityContainer}>
                <div className={homeStyle.specialityImage}>
                    {/* eslint-disable-next-line @next/next/no-img-element */}
                    <img src={'/images/ford-mustang-carzloc.webp'}
                        alt="Carzloc - Ford mustang rouge" />
                </div>
                <div className={homeStyle.specialityDescription}>
                    <h2>CARZLOC LE SPÉCIALISTE DE LA LOCATION DE VÉHICULES</h2>
                    <p>
                        Spécialisée dans la location de tous types de véhicules, votre agence CARZLOC située à Nemours
                        (77) met à votre disposition pour tous vos déplacements des voitures de tourisme, utilitaires,
                        citadines et véhicules de prestige.
                    </p>
                    <p>
                        Nos 2 agences CARZLOC situées à <strong>Nemours</strong> et <strong>Montargis</strong> (Loiret)
                        à peine 60 km de Paris, vous propose ses services de location de voiture de luxe avec ou sans
                        chauffeur privé. Vous devez effectuer un simple trajet à bord d’une voiture avec chauffeur, pour
                        un transfert professionnel ou pour le plaisir de conduire un véhicule de sport comme
                        l’authentique Ford Mustang, CARZLOC répond à toutes vos demandes. Novatrice dans la location de
                        véhicules courte ou longue durée, notre équipe met tout en œuvre pour vous proposer les
                        meilleures prestations à thème au meilleur rapport qualité prix.
                    </p>
                </div>
            </div>
        </div>
    )
}

function Categories() {
    return (
        <div className={homeStyle.categories}>
            <div className="container">
                <SectionTitle>Catégories de <span>véhicules</span></SectionTitle>
                <div className={homeStyle.categoriesContainer}>
                    {categories.map((category, key) => (
                        <Category key={key} category={category} />
                    ))}
                </div>
            </div>
        </div>
    )
}

function Category({ category }) {
    return (
        <div className={homeStyle.categoryItem}>
            <div className={homeStyle.categoryImage}>
                <Link href={category.link}>
                    <a>
                        <OptimizedImage src={category.imageUrl}
                            alt={category.label}
                            title={category.label}
                            width={360}
                            height={215} />
                    </a>
                </Link>
            </div>
            <div className={homeStyle.categoryLink}>
                <Link href={category.link}>
                    <a>{category.label}</a>
                </Link>
            </div>
        </div>
    )
}

const categories = [
    {
        imageUrl: require('../public/images/categories/utilitaire.jpg'),
        label: 'Louer une voiture citadine',
        link: '/categories/citadine'
    }
    ,
    {
        imageUrl: require('../public/images/categories/compact.jpg'),
        label: 'Louer une voiture compacte',
        link: '/categories/compact'
    }
    ,
    {
        imageUrl: require('../public/images/categories/familiale.jpg'),
        label: 'Louer une voiture familiale',
        link: '/categories/familiale'
    }
    ,
    {
        imageUrl: require('../public/images/categories/berline.jpg'),
        label: 'Louer une voiture prestige',
        link: '/categories/berline'
    }
    ,
    {
        imageUrl: require('../public/images/categories/luxe.jpg'),
        label: 'Louer une voiture luxe',
        link: '/categories/luxe'
    }
    ,
    {
        imageUrl: require('../public/images/categories/utilitaire.jpg'),
        label: 'Louer une voiture utilitaire',
        link: '/categories/utilitaire'
    }
]

export default Index
