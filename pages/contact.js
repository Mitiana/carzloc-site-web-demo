import PageHeader from '../components/PageHeader';
import SectionTitle from '../components/SectionTitle';
import contactStyle from '../styles/components/Contact.module.scss'
import BingMapsReact from 'bingmaps-react';

function Contact() {
    return (
        <>
            <PageHeader title="CONTACTEZ-NOUS" backgroundImageUrl="/images/contact.webp"/>
            <div className="container">
                <SectionTitle isBigSpan={true}>PRENDRE CONTACT AVEC CAR<span>Z</span>LOC</SectionTitle>
                <div className={contactStyle.contact}>
                    <div className="row">
                        <div className="column">
                            <div className="form-group">
                              <div className="form-group">
                                  <label htmlFor="name">Nom <span className="text-danger">*</span> :</label>
                                  <input type="text" className="form-control" id="name"/>
                              </div>
                            </div>
                        </div>
                        <div className="column">
                            <div className="form-group">
                                <div className="form-group">
                                    <label htmlFor="firstName">Prénom <span className="text-danger">*</span> :</label>
                                    <input type="text" className="form-control" id="firstName"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="column">
                            <div className="form-group">
                                <div className="form-group">
                                    <label htmlFor="email">Email <span className="text-danger">*</span> :</label>
                                    <input type="email" className="form-control" id="email"/>
                                </div>
                            </div>
                        </div>
                        <div className="column">
                            <div className="form-group">
                                <div className="form-group">
                                    <label htmlFor="phone">Numéro téléphone <span className="text-danger">*</span> :</label>
                                    <input type="text" className="form-control" id="phone"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="column">
                            <div className="form-group">
                                <div className="form-group">
                                    <label htmlFor="message">Message <span className="text-danger">*</span> :</label>
                                    <textarea id="message" className="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row" style={{justifyContent: 'center'}}>
                        <button className={contactStyle.sendButton}>Envoyer</button>
                    </div>
                </div>
            </div>
            <div className={contactStyle.contactMap}>
                <ContactMap/>
            </div>
        </>
    )
}

function ContactMap() {
    return (
        <BingMapsReact bingMapsKey="AtXglfH-EAX0IxfLoD1pMq4Rhd9SIdCJmg2gvaVULsZhM5PBXu1FoL-LuzpPerp3"
                       height={500}
                       mapOptions={{
                           navigationBarMode: "square",
                       }}
                       viewOptions={{
                           center: {
                               latitude: 48.2660961,
                               longitude: 2.6992026
                           },
                           zoom: 16,
                           mapTypeId: 'canvasLight'
                       }}
                       pushPinsWithInfoboxes={
                           [{
                               center: {
                                   latitude: 48.2660961,
                                   longitude: 2.6992026
                               },
                               options: {
                                   title: 'CARZLOC',
                                   description: '29 rue des Tanneurs,77140 Nemours',
                                   visible: true
                               },
                               metadata: {
                                   title: 'CARZLOC',
                                   description: `29 rue des Tanneurs,77140 Nemours`
                               }
                           }]
                       }

        />
    )
}

export default Contact
