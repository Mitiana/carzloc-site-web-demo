import inscriptionStyle from '../styles/components/InscriptionDone.module.scss'
import { useForm } from "react-hook-form";
import OptimizedImage from '../components/OptimizedImage'
import Link from 'next/link'
import Alert from '../components/utils/Alert'
import { useState } from 'react';
import { ReturnCode } from '../enums/return-code'
import ReactLoading from 'react-loading'

const GLOBAL_ERROR = `Une erreur s'est produite durant l'opération, Veuillez réessayer ultérieurement`

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default function Recuperation() {

    const passwordIcon = require('../public/images/icon/password.webp')
    const { register, formState: { errors }, handleSubmit } = useForm();
    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(false)
    const [success, setSuccess] = useState(false)
    const [email, setEmail] = useState(null)

    const onSubmit = async (values) => {
        setError(null)
        const url = `${process.env.API_HOST}/account/recovery`
        try {
            setLoading(true)
            await sleep(3000)
            const request = await fetch(url, { method: 'POST', body: JSON.stringify(values) })
            const response = await request.json()
            if (response.code !== ReturnCode.CODE_200) {
                setLoading(false)
                console.log('response.message', response.message)
                setError(response.message ? response.message : GLOBAL_ERROR)
            }
            setLoading(false)
            setSuccess(true)
            setEmail(response.data.email)
        } catch (e) {
            setLoading(false)
            console.log('e', e)
            setError(GLOBAL_ERROR)
        }
    }

    return (
        <div className={inscriptionStyle.InscriptionDoneContainer}>
            <div className={inscriptionStyle.InscriptionDone}>
                <OptimizedImage src={passwordIcon} width={64} height={64} alt={'Email icon'} />
                <h1>Retrouver votre compte</h1>
                {!success && <RecoveryForm error={error} setError={setError} handleSubmit={handleSubmit} onSubmit={onSubmit} register={register} loading={loading}/>}
                {success && <SuccessDOM email={email}/>}
                <div className={inscriptionStyle.separator}></div>
                <Link href="/">
                    <a className="pointer text-primary">
                        <i className="fas fa-long-arrow-alt-left" /> Revenir à l&apos;accueil
                    </a>
                </Link>
            </div>
        </div>
    )
}

function RecoveryForm({error, setError, handleSubmit, onSubmit, register, loading}) {
    return (
        <>
            {error && <Alert className="text-center">{error}</Alert>}
            <p>Veuillez entrer votre adresse e-mail pour récuperer votre compte.</p>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className={inscriptionStyle.recuperation}>
                    <input type="email"
                        className="form-control"
                        placeholder="Adresse e-mail..."
                        onFocus={() => setError(null)}
                        {...register('email', { required: true })} />
                    {!loading && <button className="btn btn-primary">Récuperer</button>}
                    {loading && <ReactLoading type={'spin'} color="#1e729f" width={20} height={20} />}
                </div>
            </form>
        </>
    )
}

function SuccessDOM({email}) {
    return (
        <Alert type="success" className="text-center">Un e-mail a été envoyé à l&apos;adresse <strong>{email}</strong></Alert>
    )
}