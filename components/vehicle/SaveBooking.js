export default function SaveBooking({ loggedIn, price }) {
    if (!(loggedIn && price)) {
        return null
    }
    return (
        <button className="btn btn-primary">Procéder à la réservation</button>
    )
}