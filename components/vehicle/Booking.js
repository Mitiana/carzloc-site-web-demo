import { format } from 'date-fns'
import { useState } from 'react'
import vehicleStyle from '../../styles/components/Vehicle.module.scss'
import DatePicker from 'react-datepicker'
import ReactLoading from 'react-loading'
import Alert from '../utils/Alert'

function Booking({ car, startDate, setStartDate, endDate, setEndDate, onCheckAvailabilityCallback, setErrorMessage, setPrice, loggedIn, setShowLogin }) {
    const [loading, setLoading] = useState(false)
    
    if (!loggedIn) {

        const onConnect = () => {
            setShowLogin(true)
        }

        return (
            <div style={{marginTop: 20}}>
                <Alert type={'primary'}>
                    Vous devez vous <span className="bold pointer" onClick={onConnect}>connecter</span> pour pouvoir faire une réservation.
                </Alert>
            </div>
        )
    }

    return (
        <div className={[vehicleStyle.carBooking, vehicleStyle.lightBackground].join(' ')}>
            <h2>Réservation</h2>
            <div className="row">
                <div className="column" style={{ paddingLeft: '0' }}>
                    <DateForm dateField={startDate}
                        setDateFunction={setStartDate}
                        label={`Date et heure de départ`}
                        id={`startDate`}
                        onDateChangedHandler={
                            () => {
                                setErrorMessage(null)
                                setEndDate(null)
                                setPrice(null)
                            }
                        }
                    />
                </div>
                <div className="column" style={{ padding: 0 }}>
                    <DateForm disabled={!startDate}
                        dateField={endDate}
                        setDateFunction={setEndDate}
                        minDate={startDate}
                        label={`Date et heure de retour`}
                        id={`endDate`}
                        onDateChangedHandler={() => {
                            setErrorMessage(null)
                            setPrice(null)
                        }}
                    />
                </div>
            </div>
            <div className={'row'}>
                {loading && <ReactLoading type={'spin'} color="#1e729f" width={20} height={20} />}
                {
                    !loading &&
                    <button
                        className="btn btn-primary"
                        onClick={async () => onCheckAvailability(startDate, endDate, car, setLoading, setErrorMessage, onCheckAvailabilityCallback)}
                        disabled={!startDate || !endDate} >Vérifier la disponibilité</button>
                }
            </div>
        </div>
    )
}

async function onCheckAvailability(startDate, endDate, car, setLoading, setErrorMessage, onCheckAvailabilityCallback) {
    setErrorMessage(null)
    try {
        setLoading(true)
        await checkAvailability(startDate, endDate, car.id)
        setLoading(false)
        onCheckAvailabilityCallback()
    } catch (e) {
        setLoading(false)
        setErrorMessage(e)
    }
}

async function checkAvailability(startDate, endDate, carId) {
    if (startDate > endDate) {
        throw 'La date de départ doit être avant la date de retour'
    }
    const formattedStartDate = format(startDate, "yyyy-MM-dd HH:mm")
    const formattedEndDate = format(endDate, "yyyy-MM-dd HH:mm")
    const requestBody = JSON.stringify({
        beginDate: formattedStartDate,
        endDate: formattedEndDate,
        carId: carId
    })
    const request = await fetch(`${process.env.API_HOST}/booking/availability`, {
        method: 'POST',
        body: requestBody
    }).catch(() => {
        throw `Une erreur s'est produite lors de la récupération des données`
    })
    const response = await request.json()
    const data = response.data
    if (data) {
        const formattedExistingBeginDate = new Date(data.beginDate).toLocaleString()
        const formattedExistingEndDate = new Date(data.endDate).toLocaleString()
        throw `Ce véhicule est réservé entre ${formattedExistingBeginDate} et ${formattedExistingEndDate}`
    }
}

function DateForm({ dateField, setDateFunction, label, id, disabled, onDateChangedHandler, minDate }) {

    const onDateChanged = (date) => {
        onDateChangedHandler()
        setDateFunction(date)
    }

    return (
        <div className="form-group">
            <label htmlFor={id}>{label}</label>
            <DatePicker
                className={'form-control'}
                disabled={disabled}
                selected={dateField}
                onChange={onDateChanged}
                locale="fr"
                minDate={minDate}
                showTimeSelect
                timeFormat="p"
                timeIntervals={30}
                dateFormat="Pp"
                id={id}
                placeholderText={label}
            />
        </div>
    )
}

DateForm.defaultProps = {
    disabled: false,
    onDateChangedHandler: () => { },
    minDate: new Date()
}

export default Booking
