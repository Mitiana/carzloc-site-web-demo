import vehicleStyle from '../../styles/components/Vehicle.module.scss'
import CurrencyValue from '../utils/CurrencyValue'
import bookingSummaryStyle from '../../styles/components/BookingSummary.module.scss'
import { formatDateTimeToString, dateDiff } from '../../components/utils/DateHelper'
const DAILY_PRICE_ID = 12

function BookingSummary({ price, beginDate, endDate, options }) {
    if (!(price && beginDate && endDate)) {
        return null
    }

    const formattedBeginDate = formatDateTimeToString(beginDate)
    const formatterEndDate = formatDateTimeToString(endDate)
    const daysNumber = dateDiff(beginDate, endDate)
    const total = calculateTotal(options, price, daysNumber)

    const optionsDOM = options.map(
        option => {
            return <Option option={option} daysNumber={daysNumber} key={option.id} />
        }
    )

    return (
        <div className={vehicleStyle.lightBackground}>
            <h2>Récapitulation</h2>
            <div className={bookingSummaryStyle.bookingSummary}>
                <div>
                    Réservation du {formattedBeginDate} au {formatterEndDate} ({daysNumber} jours)
                </div>
                <div className={bookingSummaryStyle.bookingSummaryValue}>
                    <CurrencyValue value={daysNumber * price.price} />
                </div>
            </div>
            {optionsDOM}
            <div className={[bookingSummaryStyle.bookingSummary, bookingSummaryStyle.bookingSummaryTotal].join(' ')}>
                <div>
                    Total
                </div>
                <div className={bookingSummaryStyle.bookingSummaryValueTotal}>
                    <CurrencyValue value={total} />
                </div>
            </div>
        </div>
    )
}

function calculateTotal(options, price, daysNumber) {
    let total = daysNumber * price.price
    options.map(
        option => {
            let optionPrice = option.price
            if (option.priceType.id === DAILY_PRICE_ID) {
                optionPrice *= daysNumber
            }
            total += optionPrice
        }
    )
    return total
}

function Option({ option, daysNumber }) {
    let price = option.price
    if (option.priceType.id === DAILY_PRICE_ID) {
        price *= daysNumber
    }
    return (
        <div className={bookingSummaryStyle.bookingSummary}>
            <div>
                {option.label}
            </div>
            <div className={bookingSummaryStyle.bookingSummaryValue}>
                <CurrencyValue value={price} />
            </div>
        </div>
    )
}

export default BookingSummary