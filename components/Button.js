function Button({label, type, disabled, onclickHandler}) {
    return (
        <button className={'btn btn-' + type}
                disabled={disabled}
                onClick={onclickHandler}>
            {label}
        </button>
    )
}

export default Button

Button.defaultProps = {
    label: 'Envoyer',
    type: 'primary',
    disabled: false,
    onclickHandler: () => {
        console.debug('button clicked')
    }
}
