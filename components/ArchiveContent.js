import archiveContentStyle from '../styles/components/ArchiveContent.module.scss'
import Image from 'next/image';
import Link from 'next/link'
import slugify from 'slugify'
import ReactLoading from 'react-loading'
import CustomNotFound from '../pages/404'
import { useEffect, useState } from 'react';

function ArchiveContent({ cars }) {

    if (!cars) {
        return (
            <div className={archiveContentStyle.archiveContent}>
                <div className={archiveContentStyle.archiveLoading}>
                    <ReactLoading type={'spin'} color="#1e729f" width={40} height={40} /> <span>Chargement des données</span>
                </div>
            </div>
        )
    }

    const carsDOM = [];

    for (const car of cars) {
        carsDOM.push(<Car key={car.id} car={car} />)
    }
    return (
        <div className={archiveContentStyle.archiveContent}>
            <ArchiveCarCount cars={cars} />
            <div className={archiveContentStyle.cars}>{carsDOM}</div>
        </div>
    )
}

function ArchiveCarCount({ cars }) {
    return (
        <>
            <div className={archiveContentStyle.archiveCarCount}>
                <h3>
                    {
                        cars &&
                        cars.length === 0 &&
                        <><span>AUCUN</span> véhicule à afficher</>
                    }
                    {
                        cars &&
                        cars.length > 0 &&
                        <><span>{cars.length}</span> véhicules disponibles</>
                    }
                </h3>
            </div>
        </>
    )
}

function Car({ car }) {
    const { categories, featuredImage, fuel, gearbox, id, name, place, power, price } = car
    const formattedPrice = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(price)
    const metas = [
        { icon: 'stm-icon-car', name: 'Catégories', value: categories[0].name },
        { icon: 'stm-icon-engine', name: 'Puissance', value: power.toString() },
        { icon: 'stm-icon-ac-max-passenger', name: 'Places', value: place },
        { icon: 'stm-icon-transmission', name: 'Boîte de vitesses', value: gearbox },
        { icon: 'stm-icon-fuel', name: 'Carburant', value: fuel },
    ]
    return (
        <div className={archiveContentStyle.car}>
            <div className={archiveContentStyle.carThumbnail}>
                {<Image src={featuredImage.data}
                    alt={'name car'}
                    width={250}
                    height={150} />}
            </div>
            <div className={archiveContentStyle.carCharacteristics}>
                <h2>{name}</h2>
                <div className={archiveContentStyle.carMetas}>
                    {metas.map((meta, key) => (<Meta key={key} meta={meta} />))}
                </div>
            </div>
            <div className={archiveContentStyle.carPrice}>
                <div className={archiveContentStyle.priceInfo}>
                    <span>A partir de</span>
                    <strong>{formattedPrice} €</strong>
                    <span>par jour</span>
                </div>
                <Link href={`/vehicule/${id}/${slugify(name, { lower: true })}`}>
                    <a className={archiveContentStyle.bookingLink}>
                        <i className="stm-icon-add_car" /> Réserver
                    </a>
                </Link>
            </div>
        </div>
    )
}

function Meta({ meta }) {
    return (
        <div className={archiveContentStyle.carMeta}>
            <div className={archiveContentStyle.metaIcon}><i className={meta.icon} /></div>
            <div className={archiveContentStyle.metaName}>{meta.name}</div>
            <div className={archiveContentStyle.metaValue}>{meta.value}</div>
        </div>
    )
}

export default ArchiveContent
