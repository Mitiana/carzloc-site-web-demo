import sectionTitleStyle from '../styles/components/SectionTitle.module.scss'

function SectionTitle({children, isBigSpan}) {
    const classList = [sectionTitleStyle.sectionTitle]
    if (isBigSpan) {
       classList.push(sectionTitleStyle.isBigSpan)
    }
    return (
        <div className={classList.join(' ')}>
            <h2>{children}</h2>
        </div>
    )
}

SectionTitle.defaultProps = {
    children: 'Section title',
    isBigSpan: false
}

export default SectionTitle
