export default TextLogo
function TextLogo({textSize, color, fontWeight}) {

    const textLogo = {
        fontFamily: 'Ropa Sans',
        fontSize: textSize,
        color: color,
        fontWeight: fontWeight
    }

    const textLogoZ = {
        color: '#68B6E2',
        fontSize: '130%'
    }

    return (
        <span style={textLogo}>
            CAR
            <span style={textLogoZ}>Z</span>
            LOC
        </span>
    )
}

TextLogo.defaultProps = {
    textSize: 16,
    color: '#000000',
    fontWeight: 600
}