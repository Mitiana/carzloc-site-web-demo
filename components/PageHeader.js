import pageHeaderStyle from '../styles/components/PageHeader.module.scss'

function PageHeader({backgroundImageUrl, title}) {
    return (
        <div className={pageHeaderStyle.pageHeader} style={{backgroundImage: `url('${backgroundImageUrl}')`}}>
            <div className="container">
                <div className={pageHeaderStyle.pageHeaderContainer}>
                    <h1>{title}</h1>
                </div>
            </div>
        </div>
    )
}

PageHeader.defaultProps = {
    backgroundImageUrl: '../images/page-header.webp',
    title: 'Page header'
}

export default PageHeader
