import OptimizedImage from './OptimizedImage'
import loginStyle from '../styles/components/Login.module.scss'
import { useForm } from "react-hook-form";
import { useState } from 'react'
import ReactLoading from 'react-loading'
import Link from 'next/link'
const TOKEN_KEY = 'CARZLOC_TOKEN'
const MAIL_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

export default function Login({ setShowLogin, setLoggedIn }) {
    const imageLogo = require('../public/images/logo-carzloc.png')
    const { register, formState: { errors }, handleSubmit } = useForm();
    const [loading, setLoading] = useState(false)
    const [errorLogin, setErrorLogin] = useState(null)

    const onSubmit = async (values) => {
        setLoading(true)
        const param = JSON.stringify(values)
        const url = `${process.env.API_HOST}/customer/login`
        try {
            const request = await fetch(url, {
                method: 'POST',
                body: param
            })
            const response = await request.json()
            if (response.data && response.data.message) {
                setErrorLogin(response.data.message)
            } else {
                const currentTime = (new Date().getTime() + (2 * 60 * 60 * 1000)).toString();
                localStorage.setItem(TOKEN_KEY, btoa(currentTime));
                setLoggedIn(true)
                setShowLogin(false)
                console.log('login success')
            }
        } catch (e) {
            console.log('erreur login', e)
        } finally {
            setLoading(false)
        }
    }

    return (
        <div className={loginStyle.loginContainer}>
            <OptimizedImage src={imageLogo}
                width={215}
                height={75}
                alt="Logo carzloc"
            />
            {errorLogin && <div className="text-danger" style={{ marginTop: 20 }}>{errorLogin}</div>}
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-group">
                    <label htmlFor="email">Nom d&apos;utilisateur</label>
                    <input type="text"
                        id="email"
                        className="form-control"
                        name="email"
                        autoComplete="email"
                        {...register("email", { required: true, maxLength: 50, pattern: MAIL_PATTERN })}
                        onFocus={() => setErrorLogin(null)}
                    />
                    <FieldErrorMessage field={errors.email} type={'required'} message={'Ce champ est obligatoire'} />
                    <FieldErrorMessage field={errors.email} type={'pattern'} message={'Veuillez saisir une adresse email valide'} />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Mot de passe</label>
                    <input type="password"
                        id="password"
                        name="password"
                        className="form-control"
                        autoComplete="current-password"
                        {...register("password", { required: true, maxLength: 30 })}
                        onFocus={() => setErrorLogin(null)}
                    />
                    <FieldErrorMessage field={errors.password} type={'required'} message={'Ce champ est obligatoire'} />
                </div>
                <div className={['form-group', loginStyle.actions].join(' ')}>
                    {loading && <ReactLoading type={'spin'} color="#1e729f" width={20} height={20} />}
                    {!loading && <button className="btn btn-primary">Se connecter</button>}
                    <button className="btn" type="button" onClick={() => setShowLogin(false)}>Fermer</button>
                </div>
                <div className={['form-group', loginStyle.links].join(' ')}>
                    <p>
                        Pas de compte?
                        <Link href={'/inscription'}>
                            <a className="bold pointer text-primary">
                                &nbsp;Inscrivez-vous <i className="fas fa-long-arrow-alt-right" />
                            </a>
                        </Link>
                    </p>
                    <Link href="/recuperation">
                        <a className="pointer text-primary">
                            Mot de passe oublié ?
                        </a>
                    </Link>
                </div>
            </form>
        </div>
    )
}

function FieldErrorMessage({ field, type, message }) {
    if (!field || field.type !== type) {
        return null
    }
    const fieldErrorStyle = {
        marginTop: 3,
        display: 'inline-block',
        fontSize: 10,
        color: '#721c24'
    }

    return (
        <span style={fieldErrorStyle}>{message}</span>
    )
}