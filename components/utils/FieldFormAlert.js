import { useEffect } from "react"

export default function FieldFormAlert({ content, condition }) {

    if (!condition) return null

    return (
        <span style={contentStyle}>{content}</span>
    )
}

const contentStyle = {
    color: '#ea4031',
    display: 'inline-block',
    marginTop: '3px',
    fontSize: 11
}