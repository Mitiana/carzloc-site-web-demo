import { format } from "date-fns";

export function dateDiff(date1, date2) {
    const diff = date2.getTime() - date1.getTime();
    return Math.ceil(diff / (1000 * 60 * 60 * 24));
}

export function formatDateTimeToString(date) {
    return format(date, 'dd/MM/yyyy, HH:mm')
}

export default dateDiff