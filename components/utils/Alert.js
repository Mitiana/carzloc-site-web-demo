import alertStyle from '../../styles/components/Alert.module.scss'

function Alert({ children, className, type }) {
    let styleToApply = [alertStyle.defaultStyle, className]
    switch (type) {
        case 'danger':
            styleToApply.push(alertStyle.alertStyle)
            break
        case 'primary':
            styleToApply.push(alertStyle.primaryStyle)
            break
        case 'success':
            styleToApply.push(alertStyle.successStyle)
            break
    }
    if (!children) {
        return null
    }

    return (
        <div className={styleToApply.join(' ')}>
            {children}
        </div>
    )
}

export default Alert

Alert.defaultProps = {
    type: 'danger',
    className: ''
}