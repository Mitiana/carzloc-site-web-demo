import modalStyle from '../../styles/components/Modal.module.scss'
import { useEffect } from 'react';

export default function Modal({ showModal, setShowModal, template }) {
    
    useEffect(() => {
        const body = document.getElementsByTagName('body')[0]
        const html = document.getElementsByTagName('html')[0]
        body.style.overflow = showModal ? 'hidden' : 'auto'
        html.style.overflow = showModal ? 'hidden' : 'auto'
    }, [showModal]);

    if (!showModal) {
        return null
    }

    return (
        <div className={modalStyle.modal} id="modalContainer" /*onClick={(e) => onContainerClicked(e, setShowModal)}*/>
            <div className={modalStyle.modalContent}>
                {template}
            </div>
        </div>
    )
}

function onContainerClicked(event, setShowModal) {
    const container = document.getElementById('modalContainer')
    if (container === event.target) {
        setShowModal(false)
    }
}