import NumberFormat from 'react-number-format';

function CurrencyValue({ value, suffix }) {
    return (
        <NumberFormat value={value}
            displayType={'text'}
            thousandSeparator={' '}
            suffix={' €'}
            renderText={(value, props) => <span {...props}>{value}{suffix}</span>} />
    )
}

CurrencyValue.defaultProps = {
    suffix: null
}

export default CurrencyValue