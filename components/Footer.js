import Link from 'next/link'
import Iframe from 'react-iframe'
import TextLogo from './TextLogo';

function Footer() {
    return (
        <>
            <footer className="footer">
                <div className="container">
                    <div className="footerContainer">
                        <CarzlocInformations/>
                        <CarzlocOffers/>
                        <CarzlocNavigations/>
                        <CarzlocFacebook/>
                    </div>
                </div>
                <div className="container">
                    <div className="footerCopyright">
                        <div className="copyright">
                            &copy; 2020 Copyright <TextLogo color={'#FFF'} fontWeight={400} textSize={20}/>
                        </div>
                        <div className="conception">
                            Conception site internet : &nbsp;
                            <a href="https://www.bew-web-angecy.fr"
                               target="_blank"
                               rel="noreferrer">
                                <span className="text-white">Bew Web</span> <span className="agency">Agency</span>
                            </a>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    )
}

export function CarzlocInformations() {
    return (
        <div className="carzlocInformations">
            <h3>CARZLOC</h3>
            <p>
                Spécialiste de la location de véhicules (tourisme & utilitaires) destinés aux particuliers et
                professionnels, CARZLOC situé dans le 77 et le 45 vous propose une variété de voitures récentes avec ou
                sans chauffeur pour vos besoins de mobilité : citadine, compact, berline, SUV, break, familiale ou luxe.
            </p>
        </div>
    )
}

export function CarzlocOffers() {
    return (
        <div className="carzlocOffers">
            <h3>NOS OFFRES DU MOMENT</h3>
            <p>Roulez en Fiat 500 Km illimité à partir de 29 €/Jour !</p>
        </div>
    )
}

export function CarzlocNavigations() {
    return (
        <div className="carzlocNavigations">
            <h3>NAVIGATIONS</h3>
            <ul>
                <li>
                    <Link href={'/nos-vehicules'}>
                        <a>
                            Nos véhicules
                        </a>
                    </Link>
                </li>
                <li>
                    <Link href={'/promotion'}>
                        <a>
                            Promotion
                        </a>
                    </Link>
                </li>
                <li>
                    <Link href={'/contact'}>
                        <a>
                            Contact
                        </a>
                    </Link>
                </li>
                <li>
                    <Link href={'/mentions-legales'}>
                        <a>
                            Mentions légales & RGPD
                        </a>
                    </Link>
                </li>
            </ul>
        </div>
    )
}

export function CarzlocFacebook() {
    return (
        <div className="carzlocFacebook">
            <h3>SUIVEZ−NOUS</h3>
            <Iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fcarzloc.fr&tabs=timeline&width=255&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=479824906182263"
                    width="255" height="130"/>
        </div>
    )
}

export default Footer
