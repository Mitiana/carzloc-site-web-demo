import archiveSidebarStyle from '../styles/components/ArchiveSidebar.module.scss'
import Button from './Button';
import Link from 'next/link'
import {useForm} from 'react-hook-form';

function ArchiveSidebar() {
    return (
        <div className={archiveSidebarStyle.archiveSidebar}>
            <SearchForm/>
            <Categories/>
        </div>
    )
}

/** SEARCH FORM */
function SearchForm() {

    const {register, handleSubmit, formState: {errors}} = useForm();
    return (
        <div className={[archiveSidebarStyle.sidebarElement, archiveSidebarStyle.searchForm].join(' ')}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <h3>Recherche</h3>
                <input type="text"
                       id="search"
                       name="searchValue"
                       className={['form-control', errors.searchValue ? 'has-error' : null].join(' ')}
                       placeholder="Rechercher un véhicule"
                       {...register('searchValue')}/>
                <Button label="Rechercher"/>
            </form>
        </div>
    )
}

function onSubmit(data) {
    console.log(data)
}

/** CATEGORIES */
function Categories() {
    return (
        <div className={[archiveSidebarStyle.sidebarElement, archiveSidebarStyle.categories].join(' ')}>
            <h3>Catégories</h3>
            <ul>
                {categories.map((category, key) => (
                    <li key={key}>
                        <Link href={category.link}>
                            <a>{category.label}</a>
                        </Link>
                    </li>
                ))}
            </ul>
        </div>
    )
}

const categories = [
    {link: '/categories/berline-sportive', label: 'Berline/Sportive'},
    {link: '/categories/citadine', label: 'Citadine'},
    {link: '/categories/compact', label: 'Compact'},
    {link: '/categories/familiale', label: 'Familiale/SUV/Break'},
    {link: '/categories/utilitaire', label: 'Utilitaire'},
]

export default ArchiveSidebar
