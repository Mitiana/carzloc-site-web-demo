import headerStyle from '../styles/components/Header.module.scss'
import Link from 'next/link';
import { useRouter } from 'next/router';
import OptimizedImage from './OptimizedImage';
const TOKEN_KEY = 'CARZLOC_TOKEN'

function Header(pageProps) {
    return (
        <header className="header">
            <TopHeader {...pageProps} />
            <CenterHeader />
            <Navbar />
        </header>
    )
}

function TopHeader(pageProps) {
    const router = useRouter()
    return (
        <div className={headerStyle.topHeader}>
            <div className="container">
                <ul>
                    <li>
                        <i className="fas fa-map-marker-alt" /> 229 rue Emile Mengin 45200 MONTARGIS - 29 Rue des
                        Tanneurs 77140 Nemours
                    </li>
                    <li>
                        <i className="far fa-clock" /> 24h24 7j/7
                    </li>
                    {
                        !pageProps.loggedIn &&
                        <li className="pointer" onClick={() => pageProps.setShowLogin(true)}>
                            <i className="fas fa-user"></i> Connexion
                        </li>
                    }
                    {
                        pageProps.loggedIn &&
                        <li className="pointer"
                            onClick={
                                () => {
                                    pageProps.setLoggedIn(false)
                                    localStorage.removeItem(TOKEN_KEY)
                                    router.push('/')
                                }
                            }>
                            <i className="fas fa-sign-out-alt"></i> Déconnexion
                        </li>
                    }
                </ul>
            </div>
        </div>
    )
}

function CenterHeader() {
    return (
        <div className={headerStyle.centerHeader}>
            <div className="container">
                <div className={headerStyle.centerHeaderContainer}>
                    <div className={headerStyle.siteLogo}>
                        <Link href="/">
                            <a>
                                <OptimizedImage width={215}
                                    height={75}
                                    alt={'Logo Carzloc'}
                                    src={require('../public/images/logo-carzloc.png')} />
                            </a>
                        </Link>
                    </div>
                    <div className={headerStyle.siteInfo}>
                        <ul>
                            <li>
                                <div>
                                    <i className="stm-icon-pin" />
                                </div>
                                <div>
                                    <div className={headerStyle.siteInfoTitle}>
                                        Adresse
                                    </div>
                                    <div className={headerStyle.siteInfoValue}>
                                        229 rue Emile Mengin 45200 MONTARGIS
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <i className="stm-icon-phone" />
                                </div>
                                <div>
                                    <div className={headerStyle.siteInfoTitle}>
                                        Téléphone
                                    </div>
                                    <div className={headerStyle.siteInfoValue}>
                                        <a href="tel:330164788486">
                                            33 (0) 1 64 78 84 86
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <i className="stm-icon-mail" />
                                </div>
                                <div>
                                    <div className={headerStyle.siteInfoTitle}>
                                        Email
                                    </div>
                                    <div className={headerStyle.siteInfoValue}>
                                        <a href="mailto:contact@carzloc.fr">
                                            contact@carzloc.fr
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

function Navbar() {
    const router = useRouter()
    return (
        <div className={headerStyle.navbar}>
            <div className="container">
                <ul className={headerStyle.menu}>
                    {items.map((item, key) => {
                        return (
                            <MenuItem item={item} activeRoute={router.pathname} key={key} />
                        )
                    })}
                </ul>
            </div>
        </div>
    )
}

function MenuItem({ item, activeRoute }) {
    const activeClass = item.link === activeRoute ? headerStyle.activeLink : headerStyle.normalLink
    return (
        <li className={activeClass}>
            <Link href={item.link} activeClassName={headerStyle.activeLink}>
                <a>{item.label}</a>
            </Link>
            {item.subItems && item.subItems.length > 0 && (
                <ul className={headerStyle.subMenu}>
                    {item.subItems.map((subItem, subItemKey) => (
                        <SubMenuItem item={subItem} key={subItemKey} />
                    ))}
                </ul>
            )}
        </li>
    )
}

function SubMenuItem({ item }) {
    return (
        <li>
            <Link href={item.link}>
                <a>
                    {item.label}
                </a>
            </Link>
        </li>
    )
}

const items = [
    {
        label: 'Accueil',
        link: '/'
    },
    {
        label: 'Nos agences',
        link: '/nos-agences'
    },
    {
        label: 'Nos véhicules',
        link: '/nos-vehicules',
        subItems: [
            {
                label: 'Citadine',
                link: '/citadine'
            },
            {
                label: 'Compact',
                link: '/compact'
            },
            {
                label: 'Familiale/SUV/Break',
                link: '/familiale'
            },
            {
                label: 'Prestige/Luxe/Berline',
                link: '/prestige'
            },
            {
                label: 'Utilitaire',
                link: '/utilitaire'
            }
        ]
    },
    {
        label: 'Actualités',
        link: '/c'
    },
    {
        label: 'Promotions',
        link: '/d'
    },
    {
        label: 'Contact',
        link: '/contact'
    },
    {
        label: 'CGL',
        link: '/f'
    },
    {
        label: 'Nos ventes',
        link: '/nos-ventes'
    }
]

export default Header
