import Slider from 'react-animated-slider';
import 'react-animated-slider/build/horizontal.css';
import slides from '../data/slider'
import Link from 'next/link'

export default function HomeSlider() {
    return (
        <div className="home-slide">
            <Slider infinite={true} autoplay={3000} className="slider-wrapper">
                {slides.map((slide, index) => <div key={index}>
                    <div
                        key={index}
                        className="slider-content"
                        style={{ backgroundImage: `url('${slide.image}')`}}>
                        <div className="inner">
                            <h3>{slide.description}</h3>
                            <h1>{slide.title}</h1>
                            <Link href={slide.url}>
                                <a>
                                    <button>{slide.button}</button>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>)}
            </Slider>
        </div>
    )
}