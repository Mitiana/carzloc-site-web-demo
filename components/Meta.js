import Head from 'next/head'

function Meta({ title, keywords, description }) {
    return (
        <Head>
            <meta charSet="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <meta name="description" content={description}/>
            <meta name="keywords" content={keywords}/>
            <title>{title}</title>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css"
                  integrity="sha512-42kB9yDlYiCEfx2xVwq0q7hT4uf26FUgSIZBK8uiaEnTdShXjwr8Ip1V4xGJMg3mHkUt9nNuTDxunHF0/EgxLQ=="
                  crossOrigin="anonymous" referrerPolicy="no-referrer"/>
        </Head>
    )
}

Meta.defaultProps = {
    title: 'Carzloc',
    description: 'Site de location de voiture à Nemours',
    keywords: 'mitiana, next js, react, website'
}

export default Meta
