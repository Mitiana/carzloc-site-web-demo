import Header from './Header'
import Meta from './Meta'
import Footer from './Footer';


function Layout(pageProps) {
    return (
        <>
            <Meta/>
            <Header {...pageProps}/>
            <main>
                {pageProps.children}
            </main>
            <Footer/>
        </>
    )
}

export default Layout
